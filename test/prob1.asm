org 0
start:
        mov ecx, 1 ; счетчик чисел
        mov edx, 0 ; сумма
        push 0 ; ограничить вывод нулем
loop:
        mov ebx, ecx ;
        mod ebx, 3 ;
        jz calc ;
        mov ebx, ecx ;
        mod ebx, 5 ;
        jz calc ;
        jmp skip ;
calc:
        add edx, ecx ;
skip:
        inc ecx ;
        cmp ecx, 1000 ; 
        je prepare_result ; то выходим из цикла
        jmp loop
prepare_result:
        mov eax, edx ; d = sum
        mod eax, 10 ; d = d mod 10
        add eax, 48 ; делаем сдвиг, чтобы число стало кодом символа числа
        push eax ;
        div edx, 10 ; sum = sum div 10
        cmp edx, 0 ; если сумма ещё не равна 0
        jnz prepare_result ; пишем ещё цифру в стек
result:
        pop eax ;
        cmp eax, 0 ; если сумма ещё не равна 0
        je end ;
	mov edx, 2 ; записываем номер порта в управляющий регистр
        out ;
        jmp result ;
end:
        hlt ; останавливаем программу