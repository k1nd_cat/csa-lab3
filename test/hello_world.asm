org 0
data:
        S: .word "Hello, world!", 0
org 10
start:
        mov esi, S ; загружаем адрес строки в eыш
loop:
        cmp (esi), 0 ; проверяем, что не достигли конца строки
        jz end_loop ; иначе заканчиваем цикл
        mov eax, (esi) ; записываем символ в аккумулятор
        mov edx, 2 ; записываем номер порта в управляющий регистр
        out ; сохраняем символ из eax в устройстве вывода
        inc esi ; инкрементируем счётчик
        jmp loop ; продолжаем цикл
end_loop:
        hlt ; останавливаем цикл