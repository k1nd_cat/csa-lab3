from __future__ import annotations

import re
import sys
from typing import Any

from isa import write_code


class Line:
    index: int
    line: str  # линия asm кода до комментария, в случае области data, это название переменной
    data: str  # в случае области data, это значение переменной
    is_command: bool = False

    def __init__(self, index: int, line: str, is_command: bool, data: str = ""):
        self.index = index
        self.line = line
        self.data = data
        self.is_command = is_command

    def __str__(self) -> str:
        return (str(self.index) + ", line: " + self.line + ", data: " + self.data +
                ", is command: " + str(self.is_command))


class Lines:
    lines: list[Line]
    label_indexes: dict[str, int]
    data_indexes: dict[str, int]

    def __init__(self) -> None:
        self.lines = []
        self.label_indexes = {}
        self.data_indexes = {}


def parse_lines(text: list[str]) -> Lines:
    label_rgx = re.compile(r"\A([a-zA-Z_]+):")
    org_rgx = re.compile(r"\Aorg([0-9]*)")
    data_rgx = re.compile(r"\A([a-zA-Z_]+):\s+\.word\s+\"(.*)\",\s+[0]\Z")

    str_index: int = 0
    is_command: bool = False

    lines = Lines()
    lines.lines = []
    lines.label_indexes = {}
    lines.data_indexes = {}

    line_number = 0
    while line_number < len(text):
        line = text[line_number].split(";", 1)[0].strip()  # убрать комменты
        if len(line) == 0:
            line_number = line_number + 1
            continue
        match = label_rgx.search(line)
        if match is not None:  # найден label
            label = match.group(1)
            match = data_rgx.search(line)
            if match is not None:  # оказывается, что не label, а data
                data = match.group(2)
                lines.lines.append(Line(str_index, label, is_command, data))  # кснстанта (строка уже разобрана на название константы (label) и ее значение (data))
                lines.data_indexes[label] = str_index  # индекс константы
                str_index = str_index + len(data) + 1  # увеличить индекс на размер константы + 1 (т.к. после константы следует 0)
            else:  # все-таки label
                lines.label_indexes[label] = str_index  # сразу после метки счетчик команд не меняется
                if label == "start":  # началась секция команд
                    is_command = True
        else:  # не найден label и не найдена data
            match = org_rgx.search(line)
            if match is not None:  # найден org
                str_index = int(text[line_number].strip().split(" ", 1)[
                                    1].strip())  # взять счетчик памяти / комманд как число после org
            else:  # найдена команда
                lines.lines.append(Line(str_index, line, is_command))
                str_index = str_index + 1  # увеличить счетчик комманд
                if line.startswith("push ") or line.startswith("pop "):
                    str_index = str_index + 1  # увеличить счетчик комманд для комманд, которые разбиваются на 2 команды

        line_number = line_number + 1

    """
    print('label_indexes: \n', lines.label_indexes, '\ndata_indexes:\n', lines.data_indexes, '\nlines:\n',
          '\n'.join(map(str, lines.lines)))
    """

    return lines


def get_data_and_instruction_memory(lines: Lines) -> tuple[list[dict[str, int]], list[dict[str, str | int]]]:

    " второй проход - заполнить память и команды "
    data_memory: list[dict[str, int]] = []
    instruction_memory: list[dict[str, str | int]] = []

    line_number = 0
    while line_number < len(lines.lines):
        line = lines.lines[line_number]
        if not line.is_command:  # константа
            chr_index = 0
            while chr_index < len(line.data):
                data_memory.append({
                    "index": line.index + chr_index,
                    "value": ord(line.data[chr_index])})  # положить в память со смещением
                chr_index = chr_index + 1
            data_memory.append({"index": line.index + chr_index, "value": 0})  # положить 0 в память со смещением
        else:
            line_parts = line.line.split(" ", 2)
            opcode: str = line_parts[0].strip()
            if opcode == "push":
                instruction_memory.append({"index": line.index, "opcode": "dec", "operand1": "esp"})
                instruction_memory.append({"index": line.index + 1, "opcode": "mov", "operand1": "(esp)",
                                           "operand2": line_parts[1]})
            elif opcode == "pop":
                instruction_memory.append({"index": line.index, "opcode": "mov", "operand1": line_parts[1],
                                           "operand2": "(esp)"})
                instruction_memory.append({"index": line.index + 1, "opcode": "inc", "operand1": "esp"})
            else:
                command: dict[str, Any] = {"index": line.index, "opcode": opcode}
                if len(line_parts) > 1:  # есть операнд 1
                    op1: str = line_parts[1]
                    if op1.endswith(","):  # убираем запятую в конце
                        op1 = op1[:-1]
                    label_index: int = lines.label_indexes.get(op1, -1)  # проверить, что указатель на метку
                    if label_index != -1:
                        command["operand1"] = label_index
                    else:
                        command["operand1"] = op1
                        if len(line_parts) > 2:  # есть операнд 2
                            op2: str = line_parts[2]
                            data_index: int = lines.data_indexes.get(op2, -1)  # проверить, что указатель на константу
                            if data_index != -1:
                                command["operand2"] = str(data_index)
                            else:
                                command["operand2"] = op2
                instruction_memory.append(command)
        line_number = line_number + 1

    return data_memory, instruction_memory


def translate(text: list[str]) -> dict[str, Any]:

    " первый проход - получить индексы меток, препарсинг строк (вычленить значимое) "
    lines: Lines = parse_lines(text)

    " второй проход - заполнить память и команды "
    data_memory: list[dict[str, int]]
    instruction_memory: list[dict[str, str | int]]
    data_memory, instruction_memory = get_data_and_instruction_memory(lines)

    return {"data_memory": data_memory, "instruction_memory": instruction_memory}


def main(src_file: str, target_file: str) -> None:
    code_source_array: list[str]
    with open(src_file, encoding="utf-8") as f:
        code_source_array = f.read().split("\n")
    code = translate(code_source_array)
    write_code(target_file, code)
    print("source LoC:", len(code_source_array), "code instr:", len(code.get("instruction_memory") or []))


if __name__ == "__main__":
    assert len(sys.argv) == 3, "Wrong arguments: translator.py <input_file> <target_file>"
    _, source, target = sys.argv
    main(source, target)
