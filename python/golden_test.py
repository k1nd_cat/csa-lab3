"""
Golden тесты транслятора ассемблера и машины.
Конфигурационнфе файлы: "golden/*.yml"
"""

import contextlib
import io
import logging
import os
import tempfile
from typing import Any

import machine
import pytest
import translator


@pytest.mark.golden_test("../golden/*.yml")
def test_translator_and_machine(golden: Any, caplog: pytest.LogCaptureFixture) -> None:
    caplog.set_level(logging.DEBUG)
    caplog.handler.setFormatter(logging.Formatter("%(levelname)s: %(message)s"))
    # чтобы совпадало с логом при запуске вне golden

    with tempfile.TemporaryDirectory() as tmp_dir_name:
        test_name = golden.out["test_name"]
        source = os.path.join(tmp_dir_name, test_name + ".asm")
        input_file = os.path.join(tmp_dir_name, test_name + ".in")
        target = os.path.join(tmp_dir_name, test_name + ".out")

        with open(source, "w", encoding="utf-8") as file:
            file.write(golden["in_source"])
        with open(input_file, "w", encoding="utf-8") as file:
            file.write(golden["in_stdin"])

        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            translator.main(source, target)
        with open(target, encoding="utf-8") as file:
            code = file.read()

        assert code == golden.out["out_code"]
        assert stdout.getvalue() == golden.out["out_stdout_t"]

        with contextlib.redirect_stdout(io.StringIO()) as stdout:
            machine.logging_freq = int(golden.out["logging_freq"] or 1)
            machine.main(target, input_file)
            assert stdout.getvalue() == golden.out["out_stdout_m"]
            assert caplog.text == golden.out["out_log_m"]
