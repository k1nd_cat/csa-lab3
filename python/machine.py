from __future__ import annotations

import logging
import sys
from enum import Enum
from typing import Any, ClassVar

from isa import INT_MAX, INT_MIN, Opcode, existing_registers, read_code

logging_freq: int = 1


class Register:
    """ Хранилище регистров """

    data: dict[str, int]

    def get(self, name: str) -> int:
        assert name in existing_registers, "register name '{}' does not exist".format(name)
        return self.data.get(name) or 0

    def put(self, name: str, value: int) -> None:
        assert name in existing_registers, "register name '{}' does not exist".format(name)
        self.data[name] = value

    def __init__(self) -> None:
        self.data = dict()
        self.data["esp"] = 2047
        """ указатель стека """


class IOController:
    _input_ports: ClassVar[list[int]] = [1]  # ruff
    _output_ports: ClassVar[list[int]] = [2]  # ruff

    _input_buffer: list[str]
    _output_buffer: list[str]

    def __init__(self, input_buffer: list[str]) -> None:
        self._input_buffer = input_buffer
        self._output_buffer = []

    def read(self, port: Any) -> str:
        assert port in self._input_ports, "Invalid input port '{}'".format(port)
        if len(self._input_buffer) == 0:
            raise EOFError()
        return self._input_buffer.pop(0)

    def write(self, port: int, value: Any) -> None:
        assert port in self._output_ports, "Invalid output port '{}'".format(port)
        self._output_buffer.append(value)

    def get_output(self) -> list[str]:
        return self._output_buffer


class DataPath:
    """ Блок обработки данных. """

    _data_memory: list[int]
    "область памяти"

    _register: Register
    "область регистров процессора"

    _data_addr: int
    "указатель на текущую ячейку памяти."

    io_controller: IOController
    "контроллер ввода-вывода"

    def __init__(self, init_memory: list[dict[str, Any]], input_buffer: list[str]) -> None:
        self._data_memory = [0] * 2048
        for m in init_memory:
            assert m.get("index", None) is not None and m.get("value", None) is not None, "internal error"
            index: int = int(str(m.get("index")))
            value: int = int(str(m.get("value")))
            self._data_memory[index] = value
        self.io_controller = IOController(input_buffer)
        self.register = Register()

    def get_register(self, register_name: str) -> int:
        return self.register.get(register_name)

    def set_register(self, register_name: str, value: int) -> None:
        self.register.put(register_name, value)

    def get_data(self) -> int:
        return self._data_memory[self._data_addr]

    def set_data(self, value: int) -> None:
        self._data_memory[self._data_addr] = value

    def set_data_addr(self, addr: int) -> None:
        self._data_addr = addr

    def get_output(self) -> list[str]:
        return self.io_controller.get_output()


class ALU:
    """ Арифметико-логическое устройство """

    _op1: int
    _op2: int
    _result: int
    _neg: bool = False
    _zer: bool = False

    def set_operand1(self, value: int) -> None:
        self._op1 = value

    def set_operand2(self, value: int) -> None:
        self._op2 = value

    def exec(self, operator: Opcode) -> None:
        match operator:
            case Opcode.ADD:
                self._result = self._op1 + self._op2
            case Opcode.SUB:
                self._result = self._op1 - self._op2
            case Opcode.INC:
                self._result = self._op1 + 1
            case Opcode.DEC:
                self._result = self._op1 - 1
            case Opcode.CMP:
                self._result = self._op1 - self._op2
            case Opcode.XOR:
                self._result = self._op1 ^ self._op2
            case Opcode.MOD:
                self._result = self._op1 % self._op2
            case Opcode.DIV:
                self._result = self._op1 // self._op2
        self._neg = self._result < 0
        self._zer = self._result == 0

    def get_result(self) -> int:
        return self._result

    def is_zero(self) -> bool:
        return self._zer

    def is_neg(self) -> bool:
        return self._neg


class MUX:
    """ Мультиплексор """

    class Source(str, Enum):
        MEM = "mem"
        REG = "reg"
        DAT = "dat"

        def __str__(self) -> str:
            return str(self.value)

    _src: Source
    _data: Any
    _data_path: DataPath

    def __init__(self, data_path: DataPath) -> None:
        self._data_path = data_path

    def input(self, src: Source, addr: Any = None) -> None:
        self._src = src
        self._data = addr

    def output(self) -> int:
        assert self._src is not None, "MUX source is not defined"
        result: int = 0
        if self._src == MUX.Source.MEM:  # вернуть из памяти
            result = self._data_path.get_data()
        elif self._src == MUX.Source.REG:  # вернуть из регистра
            result = self._data_path.get_register(self._data)
        elif self._src == MUX.Source.DAT:  # вернуть лексему
            value = self._data
            if not isinstance(value, int):
                value = int(value)
            result = value
        return result


class ControlUnit:
    instruction_memory = None
    "Память команд."

    _tick: int = 0
    "Текущее модельное время процессора (в тактах). Инициализируется нулём."

    _program_counter: int = 0
    "Счётчик команд. Инициализируется нулём."

    _data_path: DataPath
    "Блок обработки данных."

    _alu: ALU
    "Арифметико-логическое устройство"

    _mux: MUX
    "Мультиплексор"

    _index_mapping: dict[int, int]
    "соотношение индексов в памяти команд и индексов в списке program"

    def __init__(self, program: list[dict[str, Any]], data_path: DataPath, init_pc: int = 0) -> None:
        self.program = program
        self._index_mapping = dict()
        for i in range(len(program)):
            self._index_mapping[program[i]["index"]] = i
        self._program_counter = init_pc
        self._data_path = data_path
        self._alu = ALU()
        self._mux = MUX(data_path)
        self._tick = 0

    def get_tick(self) -> int:
        return self._tick

    def tick(self) -> None:
        self._tick += 1

    def _get_instr(self) -> dict[str, Any]:
        index: int = self._program_counter
        index_ = self._index_mapping.get(index)  # взять положение инструкции в массиве
        if index_ is not None:
            index = int(str(index_))
            instr: dict[str, Any] = self.program[index]
        else:
            instr = {"opcode": "nop"}
        return instr

    """ установить значение счётчика команд """
    def _set_program_counter(self, jump: int = -1) -> None:
        """ Если jump <> None, присвоить счётчику значение jump, в противном случае увеличить на единицу """
        if jump == -1:
            self._program_counter += 1
        else:
            self._program_counter = jump

    """ выполнить операцию ввода вывода """
    def _exec_instruction_io(self, instr: dict[str, Opcode]) -> None:
        opcode = instr.get("opcode")
        match opcode:
            case Opcode.IN:
                port = self._data_path.register.get("edx")
                value_str = self._data_path.io_controller.read(port)
                symbol_code: int = ord(value_str[0])
                assert INT_MIN <= symbol_code <= INT_MAX, "input token is out of bound: {}".format(symbol_code)
                self._data_path.register.put("eax", symbol_code)
                self.tick()
            case Opcode.OUT:
                value_int = self._data_path.register.get("eax")
                port = self._data_path.register.get("edx")
                self._data_path.io_controller.write(port, chr(value_int))
                self.tick()

    """ выполнить операцию управления программой """

    def _exec_instruction_flow(self, instr: dict[str, Any]) -> None:
        opcode = instr.get("opcode")
        match opcode:
            case Opcode.JMP:
                addr = instr.get("operand1", -1)
                self._set_program_counter(addr)
                self.tick()
            case Opcode.JE:
                if self._alu.is_zero():
                    instr_num = instr.get("operand1", -1)
                    self._set_program_counter(instr_num)
                else:
                    self._set_program_counter()
                self.tick()
            case Opcode.JZ:
                if self._alu.is_zero():
                    instr_num = instr.get("operand1", -1)
                    self._set_program_counter(instr_num)
                else:
                    self._set_program_counter()
                self.tick()
            case Opcode.JNZ:
                if not self._alu.is_zero():
                    instr_num = instr.get("operand1", -1)
                    self._set_program_counter(instr_num)
                else:
                    self._set_program_counter()
                self.tick()
            case Opcode.JLT:
                if self._alu.is_neg():
                    instr_num = instr.get("operand1", -1)
                    self._set_program_counter(instr_num)
                else:
                    self._set_program_counter()
                self.tick()
            case Opcode.JGT:
                if not self._alu.is_neg():
                    instr_num = instr.get("operand1", -1)
                    self._set_program_counter(instr_num)
                else:
                    self._set_program_counter()
                self.tick()
            case Opcode.HALT:
                raise StopIteration()

    """ выполнить операцию вычисления """

    def _exec_instruction_alu(self, instr: dict[str, Any]) -> None:
        opcode = instr.get("opcode")
        op1 = instr.get("operand1", None)
        if self._prepare_mux_input(op1):
            self._alu.set_operand1(self._mux.output())
            self.tick()
        op2 = instr.get("operand2", None)
        if self._prepare_mux_input(op2):
            self._alu.set_operand2(self._mux.output())
            self.tick()
        self._alu.exec(Opcode(opcode))
        self.tick()
        if opcode is not Opcode.CMP and op1 in existing_registers:
            self._data_path.set_register(op1, self._alu.get_result())
            self.tick()
            # записать результат в первый операнд, только если он является регистром

    """ Сформировать входы мультиплексора """
    """ (Работа с регистрами, хранение адресов памяти в регистрах) """

    def _prepare_mux_input(self, op: Any) -> bool:
        if op is not None:
            if op in existing_registers:
                self._mux.input(MUX.Source.REG, op)
            elif isinstance(op, str) and op[0] == "(" and op[-1] == ")":
                reg = op[1: -1]
                addr = self._data_path.get_register(reg)
                self._data_path.set_data_addr(addr)
                self.tick()
                self._mux.input(MUX.Source.MEM)
            else:
                "положить значение в шину данных"
                self._mux.input(MUX.Source.DAT, op)
            self.tick()
            return True
        return False

    """ Выполнение операции """
    def decode_and_execute_instruction(self) -> None:
        instr = self._get_instr()
        opcode_ = instr.get("opcode")
        opcode: Opcode = Opcode(opcode_)
        if opcode in Opcode.IO:
            self._exec_instruction_io(instr)
        elif opcode in Opcode.ALU:
            self._exec_instruction_alu(instr)
        elif opcode in Opcode.FLOW:
            self._exec_instruction_flow(instr)
            return
        else:
            op1 = instr.get("operand1", "")
            op2 = instr.get("operand2", None)
            match opcode:
                case Opcode.MOV:
                    self._prepare_mux_input(op2)
                    value = self._mux.output()
                    " определить куда положить"
                    if op1 in existing_registers:
                        self._data_path.set_register(op1, int(value))
                    elif isinstance(op1, str) and op1[0] == "(" and op1[-1] == ")":
                        reg = op1[1: -1]
                        addr = self._data_path.get_register(reg)
                        self._data_path.set_data_addr(addr)
                        self.tick()
                        self._data_path.set_data(value)
                    self.tick()
                # case Opcode.NOP: do nothing

        self._set_program_counter()
        self.tick()

    def __repr__(self) -> str:
        """ состояние CPU. """
        result: str = "TICK: {:6}, PC: {:6}".format(self._tick, self._program_counter)
        reg_index: int = 0
        while reg_index < len(existing_registers):
            reg: str = existing_registers[reg_index]
            result = result + ", {}: {:6}".format(reg.upper(), self._data_path.get_register(reg))
            reg_index = reg_index + 1
        return result + ", NEG: {}, ZERO: {}".format(self._alu.is_neg(), self._alu.is_zero())


def simulation(code: list[dict[str, Any]], input_tokens: list[str], data_memory: list[dict[str, Any]], pc: int) \
        -> tuple[str, int, int]:
    logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
    # чтобы совпадало с логом при запуске вне golden

    data_path = DataPath(data_memory, input_tokens)
    control_unit = ControlUnit(code, data_path, init_pc=pc)
    instr_counter = 0
    try:
        while True:
            control_unit.decode_and_execute_instruction()
            if instr_counter % logging_freq == 0:  # возможность уменьшения кол-ва лога при проверке из golden
                logging.debug("%s", control_unit)
            instr_counter += 1
    except EOFError:
        logging.warning("Input buffer is empty!")
    except StopIteration:
        pass

    return "".join(data_path.get_output()), instr_counter, control_unit.get_tick()


def main(instr_file: str, in_file: str) -> None:
    """
    Функция запуска модели процессора. Параметры -- имена файлов с машинным
    кодом и с входными данными для симуляции.
    """
    code: dict[str, list[dict[str, Any]]]
    code, start_pc = read_code(instr_file)
    with open(in_file, encoding="ascii") as file:
        input_text = file.read()
        input_token = []
        for char in input_text:
            input_token.append(char)

    instruction_memory: list[dict[str, Any]] = code.get("instruction_memory") or []
    data_memory: list[dict[str, Any]] = code.get("data_memory") or []
    output, instr_counter, ticks = (
        simulation(
            instruction_memory,
            input_token,
            data_memory,
            start_pc
        )
    )

    print("".join(output))
    print("instr_counter:", instr_counter, "ticks:", ticks)


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    assert len(sys.argv) == 3, "Wrong arguments: machine_.py <code_file> <input_file>"
    _, code_file, input_file = sys.argv
    main(code_file, input_file)
