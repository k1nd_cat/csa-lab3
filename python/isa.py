from __future__ import annotations

import json
from enum import Enum
from typing import Any, ClassVar

INT_MIN = -2147483648
INT_MAX = 2147483647

jump_opcodes = ["je", "jmp", "jz", "jnz", "jlt", "jgt"]
one_operand_opcodes = ["inc", "dec", "push", "pop", *jump_opcodes]
two_operand_opcodes = ["mov", "add", "sub", "xor", "cmp", "mod", "div"]
addressed_opcodes = [*one_operand_opcodes, *two_operand_opcodes]
unaddressed_opcodes = ["nop", "in", "out", "hlt"]
existing_registers = ["eax", "ebx", "ecx", "edx", "esi", "edi", "ebp", "esp"]


class Opcode(str, Enum):
    # Other Commands
    MOV = "mov"
    NOP = "nop"
    POP = "pop"  # Обрабатывается транслятором
    PUSH = "push"  # Обрабатывается транслятором

    # ALU Commands
    INC = "inc"
    DEC = "dec"
    ADD = "add"
    SUB = "sub"
    XOR = "xor"
    CMP = "cmp"
    MOD = "mod"
    DIV = "div"

    # FLOW Commands
    JMP = "jmp"
    JE = "je"
    JZ = "jz"
    JNZ = "jnz"
    JLT = "jlt"
    JGT = "jgt"
    HALT = "hlt"

    # IO Commands
    IN = "in"
    OUT = "out"

    IO: ClassVar[list[str]] = [IN, OUT]
    FLOW: ClassVar[list[str]] = [JMP, JE, JZ, JNZ, JLT, JGT, HALT]
    ALU: ClassVar[list[str]] = [INC, DEC, ADD, SUB, XOR, CMP, MOD, DIV]

    def __str__(self) -> str:
        return str(self.value)


def write_code(filename: str, code: dict[str, Any]) -> None:
    """Записать машинный код в файл."""
    with open(filename, "w", encoding="utf-8") as f:
        f.write('{\n  "data_memory": [')
        if len(code["data_memory"]) > 0:
            f.write("\n    ")
            f.write(",\n    ".join(json.dumps(i) for i in code["data_memory"]))
            f.write("\n  ")
        f.write("],\n")
        f.write('  "instruction_memory": [\n    ')
        f.write(",\n    ".join(json.dumps(i) for i in code["instruction_memory"]))
        f.write("\n  ]\n}")


def read_code(filename: str) -> tuple[dict[str, list[dict[str, Any]]], int]:
    with open(filename, encoding="utf-8") as file:
        code = json.loads(file.read())

    for instr in code.get("instruction_memory"):
        # Конвертация строки в Opcode
        instr["opcode"] = Opcode(instr["opcode"])
    start_pc = 0
    if len(code.get("instruction_memory")) > 0:
        start_pc = code.get("instruction_memory")[0].get("index", 0)
    return code, start_pc


if __name__ == "__main__":
    print("Это служебный файл.")
