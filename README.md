# csa-lab-3

- P3216, Трошкин Александр Евгеньевич
- `asm | risc | harv | hw | tick -> instr | struct | trap -> stream | port | cstr | prob1 | cache`
- Упрощенный.


## Язык программирования

### Синтаксис

**Форма бэкуса-Наура:**

``` ebnf
<программа> ::= [<секция данных>] <секция кода>

<секция данных> ::= data: <строка константы>
<строка константы> ::= <адрес> | [<метка>] .word <константы> | 
[<метка>] .word <константы> <строка константы>
<константы> ::= <константа> | <константа>, <константы>
<константа> ::= "<слово>"

<секция кода> ::= start: <программа>
<программа> ::= <строка_программы> | <строка_программы> <программа>
<строка_программы> ::= <адрес> | <метка> | [<метка>] <адресная команда> <операнд> | 
[<метка>] <адресная команда> <операнд>, <операнд> | 
[<метка>] <безадресная команда>

<метка> ::= <слово>: 
<адрес> = org <число>
<адресная команда> = ... (см. систему команд)
<безадресная команда> ::= ... (см. систему команд)
<операнд> ::= <число> | (<число>) | <метка> | (<метка>)

<слово> ::= <символ> | <слово> <символ>
<число> ::= <цифра> | <число> <цифра>
<цифра> ::= 0| 1 | 2 | .. | 8 | 9
<символ> ::= a | b | c | ... | z | A | B | C | ... | Z | _ | <цифра>

```

**Пояснение:**

Каждая непустая строка программы это одно из нижеперечисленных:

* **метка**
  * указывается название метки и двоеточие
* **константа**
  * указывается `.word` и значение в виде строки 
  * имеет название вначале
  * константа - строка: указывается строка в кавычках и нуль-терминатор через запятую
* **безадресная команда**
  * указывается только название команды
* **адресная команда**
  * указывается название команды и операнд через пробел
  * некоторые команды работают с двумя операндами 
  * операнды разделяются запятой
* **адрес**
  * указывается специальное слово `org` и адрес в десятичном формате
  * не является командой, а указывает положение кода и константы в памяти
* **комментарий**
  * пишется `;` и текст
  * комментарии не читаются программой и удаляются после компиляции

Пример программы `Hello world!`

```asm
  org 0
  data:
          S: .word "Hello, world!", 0
  org 10
  start:
          mov esi, S ; загружаем адрес строки в esi
  loop:
          cmp (esi), 0 ; проверяем, что не достигли конца строки
          jz end_loop ; иначе заканчиваем цикл
          mov eax, (esi) ; записываем символ в аккумулятор
          mov edx, 2 ; записываем номер порта в управляющий регистр
          out ; сохраняем символ из eax в устройстве вывода
          inc esi ; инкрементируем счётчик
          jmp loop ; продолжаем цикл
  end_loop:
          hlt ; останавливаем цикл
```

**Семантика:**

- Видимость данных -- глобальная
- Поддерживаются целочисленные литералы, находящиеся в диапазоне от $`-2^{31}`$ до $`2^{31}-1`$
- Поддерживаются строковые литералы, символы стоки необходимо заключить в кавычки, после строки через запятую необходимо указать `0`
- Код выполняется последовательно
- Программа обязательно должна включать метку `start:`, указывающую на 1-ю выполняемую инструкцию. Эта метка не может указывать на константу
- Программа обязательно должна включать метку `data:`, если в программе есть константы
- Название метки не должно совпадать с названием команды и не может содержать цифры
- Пустые строки игнорируются, количество пробелов в начале и конце строки не важно
- Любой текст, расположенный после символа `';'` трактуется как комментарий и до конца строки
- Память выделяется статически (2048 ячеек * 32 бит), при запуске модели.




## Организация памяти

```text
       Registers
+------------------------------+
| eax, ebx, ecx, edx           |
| esi, edi, ebp, esp           |
+------------------------------+

       Instruction memory
+------------------------------+
| 00  : program start          |
| 01  : <command 1>            |
| 02  : <command 2>            |
|    ...                       |
| n   : halt                   |
|    ...                       |
| i   : nop                    |
|    ...                       |
+------------------------------+

          Data memory
+------------------------------+
| 0000: <constant 1>           |
| 0001: <constant 2>           |
|    ...                       |
| l+0 : <str symbol 1>         |
| l+1 : <str symbol 2>         |
|    ...                       |
| 2047: <stack base>           |
+------------------------------+
```

- Память команд и данных -- раздельная
- Размер машинного слова памяти команд -- `32` бит.
- Размер машинного слова памяти данных -- `32` бит. Числа знаковые. Линейное адресное пространство. Реализуется списком чисел.
- Память данных содержит `2048` ячеек
- Адрес `2047` является указателем стека при старте процессора. Стек растет вверх. 
- Поддерживаются только прямая адресация и непосредственная загрузка.
- Данные и инструкции хранятся в том же порядке, в котором они были написаны программистом.
- Память выделяется статически, при запуске модели.
- Для хранения динамических данных программист может воспользоваться выделением памяти на стеке (команды `push` и `pop`).
- Если при написании команды указан регистр или число, будет использована прямая загрузка.
- При написании регистра в скобках, будет использована непосредственная адресация в память данных.


Программисту доступны следующие регистры:
- `eax` -- аккумуляторный регистр (accumulator register)
- `ebx` -- базовый регистр (base register)
- `ecx` -- счетный регистр (count register)
- `edx` -- регистр данных (data register), используется для адресации портов ввода/вывода
- `esi` -- указатель источника (source index register)
- `edi` -- указатель приемника (destination index register)
- `ebp` -- указатель базы стека (base pointer register)
- `esp` -- указатель стека (stack pointer register)

Также в модели существуют следующие регистры, недоступные для прямого взаимодействия:
- `pc` -- счётчик команд (program counter)
- `data_addr` -- регистр адреса (address register)

Разрядность всех регистров -- `32` бит.


## Система команд

Особенности процессора:

- Машинное слово -- 32 бит
- Доступ к памяти данных осуществляется по адресу
- Ввод и вывод осуществляется через порты
  - Порт `1` маппится на устройство ввода
  - Порт `2` маппится на устройство вывода
  - При операции ввода / вывода номер порта задаётся в регистре `edx`
  - На вывод подаётся значение из регистра `eax`, при вводе значение пишется в регистр `eax`

### Набор инструкций


| №  | Язык   | Кол-во операндов | Кол-во тактов | Описание                                                                      | Сохранение результата<br/>в первый операнд |
|:---|:-------|:-----------------|:--------------|:------------------------------------------------------------------------------|:-------------------------------------------|
| 1  | `nop`  | 0                | 1             | отсутствие операции                                                           | -                                          |
| 2  | `inc`  | 1                | 1             | увеличить значение в ячейке на 1                                              | +                                          |
| 3  | `dec`  | 1                | 1             | уменьшить значение в ячейке на 1                                              | +                                          |
| 4  | `mov`  | 2                | 1             | записать в первый операнд значение из второго                                 | +                                          |
| 5  | `add`  | 2                | 1             | сложить операнды                                                              | +                                          |
| 6  | `sub`  | 2                | 1             | вычесть из первого операнда второй                                            | +                                          |
| 7  | `xor`  | 2                | 1             | найти побитовый xor первого операнда со вторым                                | +                                          |
| 8  | `cmp`  | 2                | 1             | выставить флаги `N` и `Z` как результат вычитания второго операнда из первого | -                                          |
| 9  | `jmp`  | 1                | 1             | безусловный переход по адресу                                                 | -                                          |
| 10 | `jz`   | 1                | 1             | перейти в заданную ячейку если `Z = 1`                                        | -                                          |
| 11 | `jnz`  | 1                | 1             | перейти в заданную ячейку если `Z = 0`                                        | -                                          |
| 12 | `jlt`  | 1                | 1             | перейти в заданную ячейку если `N = 1`                                        | -                                          |
| 13 | `jgt`  | 0                | 1             | перейти в заданную ячейку если `N = 0`                                        | -                                          |
| 14 | `push` | 1                | 2             | положить значение в стек                                                      | -                                          |
| 15 | `pop`  | 1                | 2             | достать значение с вершины стека                                              | +                                          |
| 16 | `in`   | 0                | 1             | положить значение в регистр `eax` из устройства ввода (адрес порта в `edx`)   | +                                          |
| 17 | `out`  | 0                | 1             | положить значение из регистра `eax` в устройство вывода (адрес порта в `edx`) | +                                          |
| 18 | `mod`  | 2                | 2             | вычислить остаток от деления первого операнда на второй                       | +                                          |
| 19 | `div`  | 2                | 2             | вычислить целую часть от деления первого операнда на второй                   | +                                          |
| 20 | `hlt`  | 0                | 0             | остановка                                                                     | -                                          |



### Кодирование инструкций

- Машинный код сериализуется в JSON с двумя списками `data_memory` и `instruction_memory`.
- Один элемент data_memory -- один код символа строковой константы.
- константы могут занимать несколько ячеек, конец -- ячейка со значением `0`
- Один элемент `instruction_memory` -- одна инструкция.

Например, исходный код:
```assembly
org 0
data:
	S: .word "Hello, world!", 0
org 10
start:
	mov esi, S
loop:
	cmp (esi), 0
	jz end_loop
	mov eax, (esi)
	mov edx, 2
	out
	inc esi
	jmp loop
end_loop:
	hlt
```

Машинный код:
```json
{
	"data_memory": [
		{"index": 0, "value": 72},
		{"index": 1, "value": 101},
		{"index": 2, "value": 108},
		{"index": 3, "value": 108},
		{"index": 4, "value": 111},
		{"index": 5, "value": 44},
		{"index": 6, "value": 32},
		{"index": 7, "value": 119},
		{"index": 8, "value": 111},
		{"index": 9, "value": 114},
		{"index": 10, "value": 108},
		{"index": 11, "value": 100},
		{"index": 12, "value": 33},
		{"index": 13, "value": 0}
	],
	"instruction_memory": [
		{"index": 10, "opcode": "mov", "operand1": "esi", "operand2": "0"},
		{"index": 11, "opcode": "cmp", "operand1": "(esi)", "operand2": "0"},
		{"index": 12, "opcode": "jz", "operand1": 18},
		{"index": 13, "opcode": "mov", "operand1": "eax", "operand2": "(esi)"},
		{"index": 14, "opcode": "mov", "operand1": "edx", "operand2": "2"},
		{"index": 15, "opcode": "out"},
		{"index": 16, "opcode": "inc", "operand1": "esi"},
		{"index": 17, "opcode": "jmp", "operand1": 11},
		{"index": 18, "opcode": "hlt"}
	]
}
```

Где:

- `index` - адрес инструкции или данных;
- `opcode` -- строка с кодом операции;
- `operand1` -- аргумент №1;
- `operand2` -- аргумент №2;
- `value` -- значение в ячейке памяти.


Коды операций перечислены в модуле [isa](python/isa.py):
- Opcode - enum, который содержит коды операций
- Также указаны группы операций (операции управления, вычисления, ввода / вывода, другие операции)


## Транслятор

Интерфейс командной строки: `translator.py <input_file> <target_file>`

Реализовано в модуле: [translator](python/translator.py)

Этапы трансляции (функция `translate`):

1. чтение исходного кода
2. удаление пустых строк и комментариев
3. извлечение значимых частей: метки, адреса, команды и т.д.
4. валидация операндов
5. замена меток на адреса в памяти, значение адреса указывает на первую команду после метки
6. генерация слов машинного кода из инструкций
7. запись полученных слов в файл


## Модель процессора

Интерфейс командной строки: `machine.py <machine_code_file> <input_file>`

Реализовано в модуле: [machine](python/machine.py).

### DataPath
```
        ┌─────────────────┐                                                                                    
┌──────►│                 │      ┌───────────────────────────────────────────────┐                             
│       │  Control Unit   │      │                                               │                             
│       │                 │      │                                               │                             
│       └─────────────┬───┘      │                          ┌──────────────┐     │                             
│                     │          │    latch data address--->│ Data address │◄──┐ │                             
│                     │          │                          └──────┬───────┘   │ │                             
│                     ├──────────│──────────┬──────────┐           │           │ │                             
│                     │          │          │          │    ┌──────▼───────┐   │ │                             
│         ┌───────┬───│──────────│──────────│─┬────────│────┤              │   │ │                             
│         │       │   │          │          │ │        │    │              │   │ │                             
│         │ ┌─────│─┬─│──────────┴────────┬─│─│──────┐ │    │              │   │ │                             
│         │ │     │ │ │                   │ │ │      │ │    │              │   │ │                             
│         │ │     │ │ │        ┌────────┐ │ │ │ sel ┌▼─▼┐   │ Data memory  │   │ │                             
│    sel ┌▼─▼┐   ┌▼─▼─▼┐ sel   │        │ │ │ │ --->│MUX│   │              │   │ │                             
│    --->│MUX│   │ MUX │<---   │  ┌───┐ │ │ │ │     └─┬─┘   │              │   │ │                             
│        └─┬─┘   └──┬──┘       │  │   │ │ │ │ │       │     │              │   │ │                             
│          │        │          │  │  ┌▼─▼─▼─▼─▼┐ sel  └────►│              │   │ │                             
│          │        │          │  │  │   MUX   │<---        └──────────────┘   │ │                             
│        ──▼───  ───▼──        │  │  └────┬────┘                               │ │                             
│        \     \/     /        │  │       │                 ┌──────────────┐   │ │                             
│         \   ALU    /<--exec  │  │       │                 │              ├───┘ │                             
│      ┌──┬┬──┐     /          │  │       │                 │              │     │                             
│      │N ││ Z├──┬──           │  │       └────────────────►│  Registers   ├─────┘                             
│      └┬─┴┴─┬┘  │             │  │                         │              │                                   
│       │    │   │             │  │                         │              │                                   
│       │    │   │             │  │                         ├─────┬──┬─────┤                                   
└───────┴────┘   └─────────────┘  │                         │ eax │  │ edx │                                   
                                  │                         └──┬──┘  └──┬──┘                                   
                                  │                            │        │                                      
                                  │                            │        │                                      
                                  │                          output   device                                   
                                  │                          token     port                                    
                                  │                            │        │                                      
                                  │                        ┌───▼────────▼──┐                                   
                                  │                        │               │<--read                            
                                  └─────────────────input──┤ IO Controller │                                   
                                                    token  │               │<--write                           
                                                           └──▲─────────+──┘                                   
                                                              │         |                                      
                                                              |         │                                      
                                                           +--+---+ +---▼--+                                   
                                                           |input | |output|                                   
                                                           +------+ +------+                                   
```


Реализован в классе `DataPath`.

`data_memory` -- однопортовая память, поэтому либо читаем, либо пишем.

Операции чтения / записи выполняются в методах

- `set_data_addr` -- защелкнуть выбранное значение в `data_addr`
- `set_data` -- защелкнуть значение в ячейке памяти, адресуемой в `data_addr`
- `get_data` -- прочитать значение из ячейки памяти, адресуемой в `data_addr`
- `set_register` -- защелкнуть значение в заданном регистре
- `set_register` -- прочитать значение из заданного регистра

Каждый метод обрабатывается за 1 такт 

Флаги:

- `negative` -- отражает наличие отрицательного значения в аккумуляторе.
- `zero` -- отражает наличие нулевого значения в аккумуляторе.

### ControlUnit

```
       +-------+       +---------value--------+   +-------+          
       | tick  |       |                      |   |       |          
       |counter|<--+   |                    +-▼---▼-+     |          
       +---+---+   |   |                    |       |     |          
           |       |   |          select--->|  MUX  |     |          
           |       |   |                    |       |     |+1        
           |    ┌──+───+──────┐             +---+---+     |          
           |    │             │                 |         |          
           |    │             │                 |         |          
           +--->│             │             +---▼---+     |          
                │ Instruction │             |program|     |          
┌──────────────►│   decoder   │             |counter+-----+          
│               │             │             |       |                
│          ┌───►│             │             +---+---+                
│          │    │             │                 |                    
│          │    └──┬──────────┘                 |                    
│          │       │                            |                    
│          │       │                            |                    
│     Z/N flags    │                       instruction               
│          │       │                         number                  
│          │       │                            |                    
│          │    ┌──▼──────────┐                 |                    
│          │    │             │                 |                    
│          │    │             │                 |                    
│          └────┤  Data path  │                 |                    
│               │             │                 |                    
│               │             │                 |                    
│               └─────────────┘                 │                    
│                                     ┌─────────▼────┐               
│        latch instruction address--->│              │               
│                                     │ Instruction  │               
│                                     │   memory     │               
└───────────instruction───────────────┤              │               
                                      └──────────────┘               
```

Реализован в классе `ControlUnit`.

- Hardwired (реализовано полностью на Python).
- Метод `decode_and_execute_instruction` моделирует выполнение полного цикла инструкции (1-2 такта процессора).
- Операции разных типов выполняются в разных методах:
  - `_exec_instruction_io` обрабатывает команды ввода / вывода
  - `_exec_instruction_alu` обрабатывает вычислительные команды
  - `_exec_instruction_flow` обрабатывает команды, изменяющие счётчик команд


Сигнал:

- `_set_program_counter` -- сигнал для обновления счётчика команд в ControlUnit.

Особенности работы модели:

- Цикл симуляции осуществляется в функции `simulation`.
- Шаг моделирования соответствует одной инструкции с выводом состояния в журнал.
- Для журнала состояний процессора используется стандартный модуль `logging`.
- Остановка моделирования осуществляется при:
    - исключении `EOFError` -- если нет данных для чтения из порта ввода;
    - исключении `StopIteration` -- если выполнена инструкция `halt`.

### MUX
Осуществляет работу мультиплексора.

Метод `output` обрабатывает значения на вход и возвращает результат

### ALU
Осуществляет работу ALU

Метод `exec` осуществляет выполнение операции в ALU и назначает флаги N и Z

Методы `get_result`, `is_zero`, `is_neg` возвращают результат операции, значение zero-флага и negative-флага соответственно


## Тестирование


Реализованные программы

1. [hello world](test/hello_world.asm): вывести на экран строку `'Hello World!'`
2. [cat](test/cat.asm): программа `cat`, повторяем ввод на выводе.
3. [hello_user](test/hello_user.asm) -- программа `hello_user`: запросить у пользователя его
   имя, считать его, вывести на экран приветствие
4. [prob1](test/prob1.asm): Найти сумму всех чисел, кратных 3 или 5, от 1 до 1 000 000.


Интеграционное тестирование осуществляется через golden tests, конфигурация которых лежит в папке [golden](/golden).

## CI
[.gitlab-ci](/.gitlab-ci.yml)

``` yaml
ac-lab3:
  stage: test
  image:
    name: ryukzak/python-tools
    entrypoint: [""]

  before_script:
    - pip install -U pytest
    - pip install -q --no-cache-dir --progress-bar off "poetry==1.6.1" 
    - pip install pytest-golden
    - poetry install 
    - pip install ruff
    - pip install mypy

  script:
    - ruff check
    - poetry run ruff format
    - poetry run ruff format --check
    - poetry run mypy .
    - pytest -vv
```


Пример использования и журнал работы процессора на примере `cat`:

``` shell
org 0
start:
loop:
        ; считывание и вывод входных данных
        mov edx, 1 ; записываем номер порта в управляющий регистр
        in ; загружаем ввод в eax
        cmp eax, 0 ; проверяем, что не достигли конца строки
        jz end_loop ; иначе заканчиваем цикл
        mov edx, 2 ; записываем номер порта в управляющий регистр
        out ; сохраняем символ из eax в устройстве вывода
        jmp loop ; продолжаем цикл
end_loop:
        hlt

$ python ./translator.py test/cat.asm test/cat.m
source LoC: 13 code instr: 8

$ cat ./cat.m
{
  "data_memory": [],
  "instruction_memory": [
    {"index": 0, "opcode": "mov", "operand1": "edx", "operand2": "1"},
    {"index": 1, "opcode": "in"},
    {"index": 2, "opcode": "cmp", "operand1": "eax", "operand2": "0"},
    {"index": 3, "opcode": "jz", "operand1": 7},
    {"index": 4, "opcode": "mov", "operand1": "edx", "operand2": "2"},
    {"index": 5, "opcode": "out"},
    {"index": 6, "opcode": "jmp", "operand1": 0},
    {"index": 7, "opcode": "hlt"}
  ]
}

$ python ./machine.py ./cat.m ./test/cat.in
DEBUG: TICK:      3, PC:      1, EAX:      0, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:      5, PC:      2, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     11, PC:      3, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     12, PC:      4, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     15, PC:      5, EAX:    116, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     17, PC:      6, EAX:    116, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     18, PC:      0, EAX:    116, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     21, PC:      1, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     23, PC:      2, EAX:    101, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     29, PC:      3, EAX:    101, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     30, PC:      4, EAX:    101, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     33, PC:      5, EAX:    101, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     35, PC:      6, EAX:    101, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     36, PC:      0, EAX:    101, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     39, PC:      1, EAX:    101, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     41, PC:      2, EAX:    115, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     47, PC:      3, EAX:    115, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     48, PC:      4, EAX:    115, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     51, PC:      5, EAX:    115, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     53, PC:      6, EAX:    115, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     54, PC:      0, EAX:    115, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     57, PC:      1, EAX:    115, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     59, PC:      2, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     65, PC:      3, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     66, PC:      4, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     69, PC:      5, EAX:    116, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     71, PC:      6, EAX:    116, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     72, PC:      0, EAX:    116, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     75, PC:      1, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     77, PC:      2, EAX:     32, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     83, PC:      3, EAX:     32, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     84, PC:      4, EAX:     32, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     87, PC:      5, EAX:     32, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     89, PC:      6, EAX:     32, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     90, PC:      0, EAX:     32, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     93, PC:      1, EAX:     32, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:     95, PC:      2, EAX:     99, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    101, PC:      3, EAX:     99, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    102, PC:      4, EAX:     99, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    105, PC:      5, EAX:     99, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    107, PC:      6, EAX:     99, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    108, PC:      0, EAX:     99, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    111, PC:      1, EAX:     99, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    113, PC:      2, EAX:     97, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    119, PC:      3, EAX:     97, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    120, PC:      4, EAX:     97, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    123, PC:      5, EAX:     97, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    125, PC:      6, EAX:     97, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    126, PC:      0, EAX:     97, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    129, PC:      1, EAX:     97, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    131, PC:      2, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    137, PC:      3, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    138, PC:      4, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    141, PC:      5, EAX:    116, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    143, PC:      6, EAX:    116, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    144, PC:      0, EAX:    116, EBX:      0, ECX:      0, EDX:      2, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
DEBUG: TICK:    147, PC:      1, EAX:    116, EBX:      0, ECX:      0, EDX:      1, ESI:      0, EDI:      0, EBP:      0, ESP:   2047, NEG: False, ZERO: False
WARNING: Input buffer is empty!


test cat
instr_counter: 57 ticks: 147
```

Пример проверки исходного кода:

``` shell
$ poetry run pytest . -v
============================= test session starts =============================
platform win32 -- Python 3.12.1, pytest-8.2.2, pluggy-1.5.0 -- C:\Users\sasha\AppData\Local\Programs\Python\Python312\python.exe
cachedir: .pytest_cache
rootdir: C:\projects\university\ITMO_PI\sem4\АК\csa-lab3-final\csa_lab3
configfile: pyproject.toml
plugins: golden-0.2.2
collecting ... collected 4 items

python/golden_test.py::test_translator_and_machine[../golden/cat.yml] PASSED [ 25%]
python/golden_test.py::test_translator_and_machine[../golden/hello_user.yml] PASSED [ 50%]
python/golden_test.py::test_translator_and_machine[../golden/hello_world.yml] PASSED [ 75%]
python/golden_test.py::test_translator_and_machine[../golden/prob1.yml] PASSED [100%]

============================== 4 passed in 0.47s ==============================

```

| ФИО                          | алг        | LoC | code байт | code инстр. | инстр. | такт. | вариант                                                                           |
|:-----------------------------|:-----------|:----|:----------|:------------|:-------|:------|:----------------------------------------------------------------------------------|
| Трошкин Александр Евгеньевич | hello_user | 38  | -         | 24          | 346    | 1063  | `asm \| risc \| harv \| hw \| instr \| struct \| stream \| port \| cstr \| prob1` |
| Трошкин Александр Евгеньевич | cat        | 13  | -         | 8           | 57     | 147   | `asm \| risc \| harv \| hw \| instr \| struct \| stream \| port \| cstr \| prob1` |
| Трошкин Александр Евгеньевич | prob1      | 37  | -         | 32          | 10087  | 35499 | `asm \| risc \| harv \| hw \| instr \| struct \| stream \| port \| cstr \| prob1` |
